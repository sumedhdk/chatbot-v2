
import os
import discord
from dotenv import load_dotenv
import subprocess
#Done with imports



def comment_check(repo):
	os.chdir("./"+repo)
	os.system("git init")
	comment = subprocess.check_output("git log --pretty=format:'%s' -1", shell = True)
	comment = str(comment)
	comment = comment[2:-1]
	comments = ["repeat", "remember", "understand", "write without functions", "write with functions", "write with function arrays structures"]
	if comment in comments:
		return "Comment is valid"
	return "Comment invald"	




def get_repo_user_file(url):
    url = url[8:]
    url_lst = url.split( '/')
    lst_len = len(url_lst)
    if(lst_len == 6):
        user = url_lst[1]
        repo = url_lst[2]
        file_n = url_lst[3]
        return user, repo, file_n
    print("Wrong url")
    return "", "", ""


def check_file_name(file_n):
	names = ["hello.c", "add.c", "distance.c", "sumofndifferentnumbers.c", "addtwofractions.c", "addnfractions.c"]
	if file_n in names:
		return True
	return False





def clone_it(repo, user):
	clone = "git clone https://CB084:Jeev2019@bitbucket.org/"
	clone = clone+user+"/"+repo+".git"
	response = os.system(clone)
	return response

def check_clone(repo, user, file_n):
	r = 0
	if check_file_name(file_n):
		is_there = os.path.isdir("./"+repo_name)
		if is_there:
			r = os.system("rm -rf "+repo_name)
			if r == 0:
				print("Removing existing repo successful!")
			else:
				print("Had a problem removing the existing repo")
				return "Removing(deleting) repo issue"
		r = clone_it(repo, user)
		if r == 0:
			print("The updated directory has been successfully downloaded to the local system")
			return "Clone Success"
		else:
			print("Clone issue")
			return "Clone Fail"
	return "Wrong file name"	






def check_url(url):
	if url[0:22] == 'https://bitbucket.org/':
		return True
	return False

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')

client = discord.Client()

@client.event
async def on_ready():
	for guild in client.guilds:
		if guild.name == GUILD:
			break
	print(
			f'{client.user} is connected to the following guild:\n'
			f'{guild.name}(id: {guild.id})'
		)

@client.event
async def on_message(msg):
	if msg.author == client.user:
		return
	if msg.content.lower() in ["hi", "hello", "hi!", "hello!"]:
		response = "Hello! {}".format(msg.author)
		await msg.channel.send(response)
		return
	if check_url(msg.content):
		url = msg.content
		user, repo, file_n = get_repo_user_file(url)
		status = check_clone(repo, user, file_n)
		if file_n == "":
			await msg.channel.send("Enter the url to the program file!")	
		await msg.channel.send(status)
		status = comment_check(repo)
		await msg.channel.send(status)



client.run(TOKEN)

