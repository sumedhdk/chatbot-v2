import os
def check_net():
        r = os.system("ping -c 1 8.8.8.8")
        return r

def get_repo_name(url):
        url = url[8: :1]
        lst = list(url.split( '/'))
        s = len(lst)
        if(s == 6):
                repo = lst[2]
        else:
                repo = lst[2][0:-4:1]
        return repo

def clone_it(url):
        r = os.system("git clone {}".format(url))
        return r

def pull_it(path):
        os.chdir(path)
        r = os.system('git pull')
        return r

def clone_or_pull(path, url):
        is_there = os.path.isdir('./'+path)
        r = 1
        if (is_there):
                r = pull_it(path)
        else :
                r = clone_it(url)
        if (r == 0):
                print("The updated directory has been successfully downloaded to the local system")
        else:
                print("***ERROR***")

def main():
        url = 'https://sumedhdk@bitbucket.org/CB084/chatbot-v2.git'
        path = get_repo_name(url)
        clone_or_pull(path, url)

if (check_net() == 0):
        main()
else :
        print("******NO INTERNET CONNECTION******")

#https://bitbucket.org/CB084/chatbot-v2/src/master/web_status_checker.py

